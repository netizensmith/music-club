package uk.org.netizensmith.music_club.service;

import org.springframework.stereotype.Service;
import uk.org.netizensmith.music_club.domain.Member;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by brad on 02/10/16.
 */
@Service
public class MemberService {
    private List<Member> memberList = new LinkedList<Member>();
    MemberService(){
        Member m1 = new Member();
        m1.setForename("Bradley");
        m1.setSurname("Smith");
        m1.setAka("Brads");
        memberList.add(m1);
        Member m2 = new Member();
        m2.setForename("Steve");
        m2.setSurname("Miller");
        m2.setAka("Steve");
        memberList.add(m2);
        Member m3 = new Member();
        m3.setForename("Dave");
        m3.setSurname("Morris");
        m3.setAka("Morris");
        memberList.add(m3);
        Member m4 = new Member();
        m4.setForename("Dave");
        m4.setSurname("Ganner");
        m4.setAka("Gann");
        memberList.add(m4);
        Member m5 = new Member();
        m5.setForename("Phil");
        m5.setSurname("Jenkins");
        m5.setAka("Phil");
        memberList.add(m5);
    }

    public List<Member> findAll(){
      return memberList;
    }
    public void add(Member member){
        memberList.add(member);
    }
}
