package uk.org.netizensmith.music_club.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import uk.org.netizensmith.music_club.domain.Member;
import uk.org.netizensmith.music_club.service.MemberService;

import javax.validation.Valid;
import java.util.List;

@Controller
public class MemberController {
  @Autowired
  private MemberService memberService;
  @RequestMapping("/member/list")
  public void memberList(Model model) {
    List<Member> memberList = memberService.findAll();
    model.addAttribute("memberList", memberList);
  }

    @RequestMapping("/member/add")
    public void memberAdd() {
    }

    @RequestMapping(value="/member/add", method=RequestMethod.POST)
    public String memberAddSubmit(@ModelAttribute("member") @Valid Member member, BindingResult result) {
        if(result.hasErrors()){
            return "member/add";
        }
    memberService.add(member);
    return "redirect:/member/list";
  }
}
