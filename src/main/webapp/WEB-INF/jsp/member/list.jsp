<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>

	<h1>Members</h1>

	<c:forEach items="${memberList}" var="member">
		<p>
			${member.forename} ${member.surname}, aka ${member.aka}
		</p>
	</c:forEach>

</body>
</html>