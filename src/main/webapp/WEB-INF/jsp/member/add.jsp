<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<body>
<h1>Add a member</h1>

<form:form method="POST" modelAttribute="member">
	<p>
		Forename:
		<input type="text" name="forename" />
		<form:errors path="forename" cssclass="error"></form:errors>
	</p>
	<p>
		Surname:
		<input type="text" name="surname" />
		<form:errors path="surname" cssclass="error"></form:errors>
	</p>
	<p>
		Aka:
		<input type="text" name="aka" />
	</p>

	<input type="submit" />
</form:form>
</body>
</html>